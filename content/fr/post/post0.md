---
date: 2022-06-27
featured_image: '/images/thumbnails/thumb0.jpg'
tags: ["Site web"]
title: "Hugo et GitLab Pages : Génèse du site"
show_reading_time: true

---

Le blog est un projet personnel que j'avais déjà commencé en 2018 ! A l'époque, j'avais utilisé Jekyll, un générateur de site web statique pour obtenir un petit site sympathique. Malheureusement, le manque de temps personnel, notamment avec ma vie associative et les projets universitaires, m'avait empêché de le mettre à jour régulièrement, et étant tombé en désuétude, je l'ai fermé. 

Mais la situation est différente aujourd'hui, bien que l'envie soit la même. Maintenant dans le monde du travail depuis plus de deux ans, avec plus de temps libre qu'avant et des connaissances plus amples, j'ai de quoi remplir un blog sur mes petites expériences de code. Le site revit, et j'en suis fier !

Et donc mon premier sujet du blog, car en réalité je suis en plein dedans à l'heure actuelle vu que, au moment où j'écris ces mots, je suis en pleine initialisation du site, va être sa technologie.

# Pourquoi j'ai choisi Hugo face à Jekyll ?

Parlons d'Hugo ! Comme Jekyll, il s'agit d'un générateur de site statique, mais lui écrit en Go (Jekyll est écrit en Ruby). Les deux générateurs sont concurrents pour prendre la première place des générateurs de sites statiques, les deux étant très populaires parmis les développeurs cherchant à obtenir des sites rapidement et disposables sur GitLab Pages (mon hébergeur !) ou GitHub Pages (où j'hébergeais mon ancien site). Les deux présentent des avantages, que ce soit dans leurs utilisations, possibilités, facilité d'accès. Alors, on va regarder rapidement pourquoi avoir pris Hugo, pourquoi je vous le conseille, pourquoi je ne vous le conseille pas aussi ! 

## Facilité d'emploi

Quand je parle de facilité d'emploi, je regarde plusieurs aspects : est-ce que l'installation du logiciel est facile ? Est-ce que l'emploi quotidien est fastidieux ? Est-il facile de trouver des guides sur Internet pour correctement employer les outils ? On est ici sur une analyse d'approche, et non pas de possibilités. Pour donner le contexte, je code actuellement sous Windows ("bouuuh" j'entends, mais considérez que c'est plus rapide de produire un article entre deux usages usuels de son PC sous Windows que de démarrer un WSL, un dual boot Linux, voir pire : une VM !), mais j'ai bien entendu exploré les possibilités sous Linux, et je les prendrai en compte.

**Commençons par Jekyll**: le site web de Jekyll est assez pratique, car il donne toutes les étapes à suivre pour installer Jekyll. Mais il est assez explicite : "Bien que Windows n'est pas officiellement une plateforme supportée, il est possible de lancer Jekyll en faisant quelques ajustements". Si vous êtes sur Windows, je vous conseille fortement de passer par WSL (Windows Subsystem for Linux), qui vous permettra d'employer un terminal Ubuntu (ou toute autre distribution supportée il me semble, personnellement j'ai un Ubuntu de secours) pour suivre l'installation et esquiver les problèmes liés à Windows. Mais l'installation Linux est assez simple : d'abord, on installe Ruby via apt, puis on change l'environnement pour modifier PATH et y mettre GEM_HOME (le répertoire des gems, on verra ça plus tard). Globalement, il suffit de suivre [le tutoriel ici](http://jekyllrb.com/docs/installation/ubuntu/). Une fois installé, [la documentation](http://jekyllrb.com/docs/) est assez pratique pour démarrer rapidement. Tout est expliqué en détails dans les documents, mais le quickstart permet facilement de démarrer un site en quatre commandes. 

**Hugo, lui, est plus simple d'installation.** Au début, quand j'ai regardé comment installer Hugo, j'ai vu qu'il me fallait avoir Go. Sur Windows, les installers de Go sont bien faits et demandent aucun effort, sur Linux on l'installe manuellement dans ```/usr/go``` et on change le PATH pour inclure Go dans l'environnement. Ensuite, on peut installer Hugo : extraire une archive et mettre le contenant dans le PATH sur Windows, ou encore plus simple sur Linux, le package est disponible sur `snap`. Si vous essayez de passer par un WSL pour l'installation de Hugo, vérifiez que vos services fonctionnent correctement, ils sont souvent mal réglés en partant d'un WSL frais, et les modifier ça peut vite être embêtant.

## Extensions et développement du site

**Jekyll est une gem de Ruby**, c'est à dire un **package**. Les plugins Jekyll sont aussi des gems, et c'est l'intérêt principal de Jekyll : tout est fortement modulable avec des plugins. Vous voulez générer automatiquement des archives pour votre site : [jekyll-archives](https://github.com/jekyll/jekyll-archives) existe pour ça (cadeau : [un repo qui en liste plein](https://github.com/planetjekyll/awesome-jekyll-plugins)). On les ajoute à un Gemfile (qui liste les gems), on lance un coup de bundle, et c'est généré, et le site est employable directement. Jekyll est donc assez facile d'emploi pour savoir quoi mettre dedans, si peu on sait quoi mettre dedans.

**Hugo ne possède pas d'extension**. L'outil étant prévu pour être plus un générateur de blog qu'un générateur de site, les possibilités sont bien plus limitées qu'avec Jekyll. Rajouter des fonctionnalités avec Hugo est *possible*, mais demande bien plus d'efforts et de connaissances de web pures (soyez donc prêts à faire du JavaScript, ou au moins à inclure de l'HTML manuellement).

Pour produire des articles, **les deux outils sont** (presque) **identiques**. Les deux emploient du Markdown pour décrire les pages web, et les deux outils offrant des mises à jour automatiques du site en local lors de la modification de fichier, il est très rapide de voir quels changements sont faits sur les pages. Jekyll sépare les brouillons des posts en deux arborescences de fichiers différentes, tandis que Hugo garde tout dans une seule arborescence, l'information est stockée dans les fichiers Markdown. Hugo offre cependant un système d'*archetypes*, comprenez "templates", qui permettent de générer des pages très rapidement avec des contenus prédéfinis en une commande dans le terminal. 

Finalement, pour produire le site, **les deux outils sont** (vraiment) **identiques**. Jekyll permet de produire un site en lançant `jekyll build` et le site est livré dans le répertoire `_site`, Hugo permet la même fonctionnalité avec des noms différents : lancer `hugo` produit le site dans `public`.

## Apparences des sites

Si les deux outils sont si populaires, c'est qu'ils permettent, en dix minutes, d'avoir des sites très jolis, et c'est possible grâce à un système de thèmes visuels très facile d'utilisation. Jekyll vient avec un thème par défaut d'apparence correcte mais très impersonnelle, tandis que Hugo ne possède pas de thème par défaut, et est donc, disons-le, moche par défaut. Une bonne motivation pour changer tout ça.

**Les thèmes de Jekyll sont des plugins.** Ca veut dire qu'il faut les installer comme tout autre plugin, et donc qu'il faut faire un `bundle` après chaque modification des thèmes. Alors, comme chaque plugin peut avoir des dépendances (d'autres gems), on peut vite se retrouver avec des thèmes lourds, complets et difficilement personnalisables.

**Les thèmes de Hugo sont des dossiers.** Il n'y a pas de notion de plugin dans Hugo, et donc il suffit juste d'indiquer dans son fichier de configuration "thème = chemin", et ça marche tout seul. Il est aussi beaucoup plus simple de modifier les fichiers, et donc de personnaliser l'apparence de son site. Je vous cache pas que c'est la raison principale de mon choix, je sais que je pourrai modifier l'apparence et donc l'attrait de mon site bien plus facilement.

## Performances

C'est bête, mais même si un site est statique et qu'il ne communique pas à un serveur, il peut quand même être lourd et faire patauger votre navigateur. Sur ce point-là, **Jekyll ne brille pas en passant à l'échelle**. Certains plugins peuveut être sympathiques sur des sites à taille rédute (pas beaucoup d'images et pas plus de pages), mais peuvent être très impactant sur des sites plus larges. Si vous souhaitez voir comment la mise à l'échelle peut être impactant pour Jekyll, je vous recommande [cet article suivant d'un autre blog](https://css-tricks.com/comparing-static-site-generator-build-times/), en anglais.

## Conclusion

Conclusion très simple, vraiment : les deux outils sont assez similaires (concurrents d'un même secteur, après tout). Jekyll est très facile à employer si on possède déjà un environnement RubyGems et qu'on a besoin du pouvoir extensif des plugins. Hugo, lui, est plus simple d'utilisation, permet plus facilement de customiser son site et est bien plus direct dans son emploi. Parait-il aussi que Hugo permet de mieux passer à l'échelle, et les temps de production des sites est grandement réduit avec Hugo, mais mon échelle sera petite pour ce site, et donc ce critère n'est pas rentré dans ma décision.

En tout cas, j'espère que ce rapide post vous a plu ! En réalité, je l'ai surtout écrit pour avoir un article concret sur le site et voir comment il fonctionne globalement par rapport à Jekyll, mais au final c'était amusant à écrire et ça m'a permis de mettre des mots fixes sur mes pensées !

Bonne journée à vous !

#### Sources (c'est important !)

* Comment installer Go : [https://go.dev/doc/install](https://go.dev/doc/install)
* Comment installer Hugo : [https://gohugo.io/getting-started/installing/](https://gohugo.io/getting-started/installing/)
* Comment installer Jekyll (il fait Ruby + Jekyll) : [http://jekyllrb.com/docs/installation/](http://jekyllrb.com/docs/installation/)