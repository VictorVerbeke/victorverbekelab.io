---
date: 2023-06-01
featured_image: '/images/thumbnails/thumb2.jpg'
tags: ["Python", "Code"]
title: "Python, bitwise et structures natives du langage"
show_reading_time: true

---
Le Python, c'est super cool ! Vitesse de prototypage phénoménale, syntaxe simple à appréhender et code aéré, concepts objets relativement bien pensés et une communauté super active.
Ca semble séduisant, mais comme d'habitude, chaque langage a ses tares, et aujourd'hui, on va passer cinq minutes sur un problème qui m'aura pris cinq heures durant mon travail : les opérations bitwise en Python.

## Opération bitwise ?
Les opérations bitwises, aussi connue en français sous le nom d'opérations bit à bit, c'est des opérations qui agissent sur chacun des bits d'une variable plutôt que d'agir sur la variable en elle-même.
Par exemple, en C, on a les `int` (qui peuvent prendre plusieurs tailles, mais supposons ici qu'ils fassent 4 octets, donc 32 bits) : ils peuvent être représentés en tant que nombre (`49`),
en hexadécimal (`0x31` avec plein de zéros avant) ou en binaire (`0b0000...110001` avec encore plus de zéros avant).
C'est la dernière représentation ici qui nous intéresse : chaque bit possède une valeur, 1 ou 0, et accumulés, ils encodent la valeur 49.

Un exemple d'opération bitwise, c'est le `not` : tous les 0 passent à 1, et vice-versa. Nous obtenons donc `0b1111...001110` en faisant un not bitwise sur notre valeur de 49.
Il existe des opérations à une entrée (`not`) et des opérations à deux entrées (`or`, `and`, `xor`, `nor`, `xnor` notamment). Chacune suit des tables de vérités qui disent "pour le bit a et le bit b, j'attends le résultat suivant".

## Et le Python dans tout ça ?
J'y arrive ! Le Python intègre bien les opérations bitwise :
```python
a = 42
b = 1337
print(!a)	# not a
print(a^b)	# a xor b
print(a|b)	# a or b
```
Et ces opérations marchent à merveille ! Alors, quel est le problème avec le Python ? Laissez-moi vous montrer une idiotie sans nom :
```python
a = -1
print(bin(a))
```
Et là c'est la question à un million : qu'affiche le print ? La réponse peut sembler bête, mais il s'agit de `-0b1`. Et ceci, quand on a fait du C durant ses études, c'est très moche, c'est même meurtrier. Pourquoi ?

## Le complément à deux
La base pour tout programmeur bas niveau, le complément à deux est une manière d'encoder les nombres sur une taille fixe de bits, où le bit de poids fort (le plus à gauche en big-endian) définit le signe. 
Imaginons que nous encodons nos chiffres sur quatre bits, bits annotés de gauche à droite b1, b2, b3, b4
- Le bit b1 est le bit de signe, de valeur égale à `2^3 * -1`, soit -8
- Le bit b2 est de valeur `2^2`, soit 4
- Le bit b3 est de valeur `2^1`, soit 2
- Le bit b4 est de valeur `2^0`, soit 1

Pour encoder le nombre "3", il faut donc avoir la variable à `0b0011`. Pour encoder le nombre -7, il faut avoir la variable à `0b1001`. 
Et globalement, pour n'importe quelle taille donnée de bits, le chiffre -1 est encodé par une entièreté des bits à 1 (ici, `0b1111`).

L'intérêt est principalement arithmétique, et [l'article Wikipedia à ce sujet](https://fr.wikipedia.org/wiki/Compl%C3%A9ment_%C3%A0_deux) en parlera mieux que moi. Mais une des charactéristiques du complément à deux est d'encoder sur **une taille définie de bits**. En Python, il y a donc un problème de type : les entiers ne possèdent pas une taille fixe, et donc le complément à deux ne peut pas être employé pour calculer le négatif, d'où le signe `-` avant la représentation binaire.

## Les entiers en Python
En C, `int` représente un entier, taille définie souvent sur quatre octets (des fois deux). En Python, un entier est (quasiment tout le temps) une instance de la classe int, dérivant de `numbers.Integral` (qui dérive lui-même de `numbers.Rational`, qui dérive de `numbers.Real`, qui dérive de `numbers.Complex`, ça en fait de l'objet pour un chiffre entier). Et les objets en Python, et bien c'est pas implémenté par magie, tout objet dérive du type `PyObject`. L'entier en fait partie et est défini ainsi (en C, car on va se baser sur [CPython](https://github.com/python/cpython/blob/main/Include/object.h#L161)) :
```python
struct _longobject {
    PyObject_VAR_HEAD
    digit ob_digit [1];
};
```

Toute classe définie hérite de `PyObject`, et toute classe de taille variable héritent de `PyVarObject` qui rajoute une variable dans la classe pour suivre la taille allouable de l'objet. Ainsi, petite remarque sur le côté, quand on cherche à connaître la taille d'un entier en Python, on doit voir pourquoi il prend autant de place : 
- Le nombre de références d'entiers (sur 8 octets),
- La taille actuelle de l'entier en nombre de décimales (sur 8 octets),
- Le pointeur sur l'instance (4 ou 8 octets, selon l'architecture 32- ou 64-bits),
- Un tableau d'entiers consécutifs* (de 2 ou 4 octets, selon l'architecture 32- ou 64-bits).
 
Ce tableau de digit est donc la limitation technique du Python quant aux opérations bitwise : 
les tailles des entiers dépendent des valeurs stockées, et donc un objet de longueur variable ne peut pas avoir de complément à deux, on choisit donc de noter ça de manière rustre : la valeur négative en binaire naïf.

## Le problème à 2.000.000€
Bien ! Maintenant qu'on a tout ça, la question à deux millions d'euros (il faut bien surenchérir) : 
comment affiche-t-on la représentation n-bits, complément à deux, d'un nombre négatif ?

Deux solutions : la première consiste à effectuer un masque de mise à zero. Ainsi, il est possible de gérer des entiers de taille fixe, au moins en représentation binaire :
```python
a = -120
print(bin(a & 0b11111111))  # Pour du 8-bit, retourne 0b10001000 
print(bin(a & 0xFFFF))      # Pour du 16-bit, retourne 0b1111111110001000 
```
Une autre solution consiste à calculer la valeur maximale d'un entier non-signé sur n-bits, puis de retirer la valeur à cet entier (avec ici un modulo pour les cas où a serait positif) :
```python
a = -120
max_value = 2**8        # Equivalent à 0x100 - 0x001, soit 0xFF
c2_repr = bin((max_value + a) % max_value)
```

De cette manière, vous obtenez vos compléments à deux corrects ! Bon, quelqu'un pourrait se poser la question des cas où ça peut poser problèmes et je l'avoue, il n'y en a pas beaucoup : le Python n'est pas un langage de bas niveau et donc est rarement employé dans ces termes-là. Mais dans un cas de migration de bases de données où les variables ne suivent pas la logique du Python, c'est bon à savoir !

#### Sources (c'est important !)

* [Le code source de CPython, la variante la plus usée](https://github.com/python/cpython)
* [The Numeric Tower, documentation de Python sur les héritages des types numériques](https://docs.python.org/3/library/numbers.html#the-numeric-tower)
* [Le complément à deux, Wikipédia, qui en parle plutôt bien](https://fr.wikipedia.org/wiki/Compl%C3%A9ment_%C3%A0_deux)
* [La logique et l'intérêt du bitwise, page random en anglais qui en parle bien](https://www.advanced-ict.info/mathematics/bitwise.html) 
* [Les tables de vérité, base de la logique booléenne et donc bitwise, toujours Wikipédia](https://fr.wikipedia.org/wiki/Table_de_v%C3%A9rit%C3%A9)